import React, {useState} from 'react';

import "./profile.css"

export default function StudentProfile() {

    let content1 = {
        English: {
            text1: "Technical University of Cluj-Napoca",
            text2: "Address",
            text3: "Phone",
        },
        Romanian: {
            text1: "Universitatea Tehnica din Cluj-Napoca",
            text2: "Adresa",
            text3: "Telefon",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)

    var tableBooks = document.getElementById('books');
    if (tableBooks != null)
        tableBooks.remove();

    return (

        <div className="StudentProfile">

            <br/>
            <div className="profil">
                <img className="image" src={require("./img/CV_Pic.PNG")} alt="Andrei" />
                    <h1><b>Aitculesei Andrei</b></h1>
                    <p className="title">Student AC, T.I.</p>
                    <p> {content.text1} </p>
                    <div className="socialMedia">
                        <a href="https://twitter.com/aitculesei"><i className="fab fa-twitter"/> </a>
                        <a href="https://www.linkedin.com/in/aitculesei-andrei-b9451b181/"><i className="fab fa-linkedin"/> </a>
                        <a href="https://www.facebook.com/andrei.aitculesei"><i className="fab fa-facebook"/> </a>
                    </div>
                    <button className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Contact</button>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-home"/> {content.text2}: <p>Str. Teodor Mihali Nr. 4</p></a>
                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-phone"/> {content.text3}: <p>0744284365</p></a>
                        <a className="dropdown-item" href="https://mail.yahoo.com/d/compose/4989693236?.intl=ro&.lang=ro-RO&.partner=none&.src=fp"><i className="fa fa-fw fa-envelope"/> Mail: <p>andrei.aitculesei1@gmail.com</p></a>
                    </div>
            </div>
            <br/>
        </div>
    );
}
