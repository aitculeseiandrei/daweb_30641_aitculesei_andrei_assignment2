import React, {useState} from 'react';

import "./profile.css"

export default function CoordinatorProfile() {

    let content1 = {
        English: {
            text1: "Technical University of Cluj-Napoca",
            text2: "Address",
            text3: "Phone",
        },
        Romanian: {
            text1: "Universitatea Tehnica din Cluj-Napoca",
            text2: "Adresa",
            text3: "Telefon",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)

    var tableBooks = document.getElementById('books');
    if (tableBooks != null)
        tableBooks.remove();

    return (

        <div className="profil">
            <br/>
            <img className="image" src={require("./img/tiberiuMarita.jpg")} alt="Marita" />
                <h1><b>Tiberiu Marita</b></h1>
                <p className="title">Associate Professor</p>
                <p>{content.text1}</p>
                <br/>
                    <button className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" > Contact </button>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-home"/> {content.text2}: <p>Str. C. Daicoviciu, No.15</p></a>
                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-phone"/> {content.text3}: <p>(+40 264)
                            401457</p></a>
                        <a className="dropdown-item" href="https://mail.yahoo.com/d/compose/4989693236?.intl=ro&.lang=ro-RO&.partner=none&.src=fp"><i className="fa fa-fw fa-envelope"/> Mail: <p>tiberiu.marita@cs.utcluj.ro</p></a>
                    </div>
            <br/>
        </div>
    );
}
