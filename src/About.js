import React, {useState} from 'react';

import "./index.css"

export default function About() {

    let content1 = {
        English: {
            text1: "General information",
            text2: "Concepts",
            text3: "is a non-profit with the aim of facilitating the development and use of a global standard for the interface of security products based on physical IPs. Offers",
            text4: "Control over the house",
            text5: "Possibility to open the doors",
            text6: "Possibility to check your living room in real time",
            text7: "Control over the lights",
            text8: "The goal",
            text9: "is to standardize the way in which IP products (surveillance cameras, alarms, doors, audio recordings, etc.) communicate with each other, more precisely it aims to standardize how you connect to these products, for example if you develop an application for transmission. of images received from an ONVIF camera",
            text10: "Example",
            text11: "Description of the project",
            text12: "Title",
            text13: "Functionality",
            text14: "in the form of an application on the phone using the ONVIF protocol to receive images from a video camera, which would be followed by the \"image processing\" application, so that it would work on both Android and IOS and provide a more accurate replica of the existing rooms as endowments on newer cars, with the possibility of bringing news or improvements.",
            text15: "IDE",
            text16: "The goal",
            text17: "is to provide the opportunity to have assistance during parking in all possible conditions and in the case of older cars (both day and night), which do not have this facility from the factory",
        },
        Romanian: {
            text1: "Informații generale",
            text2: "Concepte",
            text3: "este non-profit cu scopul de a facilita dezvoltarea și folosirea unui standard global pentru interfața unor produse de securitate bazate pe IP-uri fizice. Oferă",
            text4: "Control asupra locuinței",
            text5: "Posibilitate de a deschide ușile",
            text6: "Posibilitate de a îți verifica în timp real camera de zi",
            text7: "Control asupra luminilor",
            text8: "Scopul",
            text9: "este de a standardiza modul in care produsele IP(camere de supraveghere, alarme, usi, inregistrari audio etc.) comunica intre ele, mai exact are ca scop standardizarea modului in care te conectezi la aceste produse, de exemplu daca dezvolti o aplicatie pentru transmisie de imagini primite de la o camera ONVIF",
            text10: "Exemplu",
            text11: "Desciere lucrare de licenta",
            text12: "Titlu",
            text13: "Functionalitate",
            text14: "sub forma unei aplicatii pe telefon folosind protocolul ONVIF pentru a primi imagini de la o camera video, asupra carora ar urma aplicarea \"procesarii de imagini\", astfel incat sa functioneze atat pe Android cat si pe IOS si sa ofere o replica cat mai precisa a camerelor existente ca dotare pe masinile mai noi, cu posibiltatea de a aduce noutati sau imbunatatiri",
            text15: "Mediu de dezvoltare",
            text16: "Scop",
            text17: "este de a oferi posibilitatea de a avea asistenta in timpul parcarii in toate conditiile posibile si in cazul masinilor mai vechi (atat pe timp de zi cat si pe timp de noapte), care nu au disponibile aceasta dotare din fabrica",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)

    var tableBooks = document.getElementById('books');
    if (tableBooks != null)
        tableBooks.remove();

    return (

        <div>

            <br/><hr/>
            <div className="Container">
                <div id="slides" className="carousel slide" data-ride="carousel">
                    <ul className="carousel-indicators">
                        <li data-target="#slides" data-slide-to="0" className="active"/>
                        <li data-target="#slides" data-slide-to="1"/>
                    </ul>

                    <div className="carousel-inner">
                        <div className="carousel-item active" align="center">
                            <img src={require("./img/ONVIF.PNG")} alt="onvif" />
                        </div>
                        <div className="carousel-item" align="center">
                            <img src={require("./img/ONVIF4IOS.PNG")} alt="onvif2" />

                        </div>
                    </div>
                </div>
            </div>
            <hr/><br/>

            <div className="Content">
                <div className="row welcome text-center">
                    <div className="col-12">
                        <h1 className="display-4"><b><i> {content.text1} </i></b></h1>
                    </div>
                </div>
                <br/>
                    <div className="Concepte">
                        <div className="col-12">
                            <h3> {content.text2}: </h3>
                            <ul>
                                <li>
                                    <p><b>ONVIF</b>(<b>O</b>pen <b>N</b>etwork <b>V</b>ideo <b>I</b>nterface)= {content.text3}:</p>
                                    <ul className="fa-ul">
                                        <li><i className="fa-li fa fa-check-square"/><b> {content.text4}; </b>
                                        </li>
                                        <li><i className="fa-li fa fa-check-square"/><b> {content.text5}; </b></li>
                                        <li><i className="fa-li fa fa-check-square"/><b> {content.text6}; </b></li>
                                        <li><i className="fa-li fa fa-check-square"/><b> {content.text7}. </b>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p><b>{content.text8}:</b> {content.text9}.</p>
                                </li>
                                <li>
                                    <p><b>{content.text10}:</b></p>
                                    <video width="400" controls>
                                        <source
                                            src={require("./img/Video/y2mate.com - ONVIF Tutorial for Android_FlPYcgj6UdU_1080p.mp4")}
                                            type="video/mp4" />
                                    </video>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="Descriere">
                        <div className="col-12">
                            <h3> {content.text11}:</h3>
                            <ul>
                                <li>
                                    <p><b>{content.text12}:</b> Car parking camera </p>
                                </li>
                                <li>
                                    <p><b>{content.text13}:</b> {content.text14}. </p>
                                </li>
                                <li>
                                    <p><b> {content.text15}:</b> Android studio/XCode</p>
                                </li>
                                <li>
                                    <p><b> {content.text16}:</b> {content.text17}. </p>
                                </li>
                            </ul>
                        </div>
                    </div>

            </div>
            <br/><hr/><br/>

        </div>
    );
}
